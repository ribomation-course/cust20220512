#include <iostream>
#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "simple-data.hxx"
using namespace std;

int main() {
    auto const N       = 1000;
    auto const size    = N * sizeof(SimpleData);
    auto       segment = mmap(0, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (segment == MAP_FAILED) {
        throw runtime_error{"mmap failed: "s + strerror(errno)};
    }

    for (auto k = 0; k < N; ++k) {
        auto addr = segment + k * sizeof(SimpleData);
        auto ptr  = new (addr) SimpleData{k + 1};
        //cout << "segment[" << k << "] @ " << ptr << "\n";
    }

    cout << "----\n";
    for (auto k = 0; k < N; ++k) {
        SimpleData* ptr = &reinterpret_cast<SimpleData*>(segment)[k];
        cout << "segment[" << k << "] = " << *ptr << "\n";
    }

    cout << "----\n";
    for (auto k = 0; k < N; ++k) {
        SimpleData* ptr = reinterpret_cast<SimpleData*>(segment + k * sizeof(SimpleData));
        ptr->~SimpleData();
    }

    if (munmap(segment, size) < 0) {
        throw runtime_error{"munmap failed: "s + strerror(errno)};
    }
}
