#include <iostream>
#include "simple-data.hxx"
using namespace std;

char buf[sizeof(SimpleData)];

int main() {
    auto ptr = new (buf) SimpleData{10};
    cout << "*ptr: " << *ptr << " @ " << ptr << "\n";

    auto p = reinterpret_cast<int*>(ptr);
    cout << "p[0]: " << p[0] << ", " << *p << "\n";
    cout << "p[1]: " << p[1] << ", " << *(p + 1) << "\n";
    cout << "p[2]: " << p[2] << ", " << *(p + 2) << "\n";
    ptr->~SimpleData();
    //delete ptr;  //this will not work!
}
