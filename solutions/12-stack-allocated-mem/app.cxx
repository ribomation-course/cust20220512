#include <iostream>
#include <string>
#include <alloca.h>

using std::cout;
using std::string;
using std::to_string;
using namespace std::literals::string_literals;

struct stk_mem_t {};
extern const stk_mem_t stk_mem;

__attribute__ ((always_inline))
inline void* operator new(size_t numBytes, stk_mem_t) {
    return ::alloca(numBytes);
}


struct Person {
    string const   name;
    unsigned const age;
    Person* const next;

    Person(string name, unsigned age, Person* next)
            : name{std::move(name)}, age{age}, next{next} {}
};

void print(Person* node) {
    if (node == nullptr) return;
    cout << "Person{" << node->name << ", " << node->age << "} @ " << node << "\n";
    print(node->next);
}

void dispose(Person* node) {
    if (node == nullptr) return;
    node->~Person();
    dispose(node->next);
}


void run(unsigned numPersons) {
    Person* list = nullptr;
    auto      prefix = "Nisse-"s;
    for (auto k      = numPersons; k > 0; --k) {
        auto name = prefix + to_string(k);
        auto age  = 20U + (k % 80);
//        auto mem  = alloca(sizeof(Person));
//        list = new (mem) Person{std::move(name), age, list};
        list = new (stk_mem) Person{std::move(name), age, list};
    }
    print(list);
    dispose(list);
}

int main(int argc, char** argv) {
    auto const N = argc == 1 ? 10U : std::stoi(argv[1]);
    run(N);
}
