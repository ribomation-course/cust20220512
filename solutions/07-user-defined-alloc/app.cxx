#include <iostream>
#include "monotonic-allocator.hxx"
#include "circular-allocator.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

void usingMonotonic() {
    cout << "-- using a monotonic allocator --\n";
    auto mem = MonotonicAllocator<40>{};
    int  n   = 0;
    try {
        while (true) {
            int* ptr = new (mem.allocate(sizeof(int))) int{n + 1};
            cout << "ptr: " << *ptr << " @ " << ptr << endl;
            ++n;
        }
    } catch (bad_alloc& x) {
        cout << "allocated " << n << " blocks, before overflow\n";
    }
}

void usingCircular() {
    cout << "-- using a circular allocator --\n";
    auto mem = CircularAllocator<20>{};
    int  n   = 0;
    while (n < 20) {
        int* ptr = new (mem.allocate(sizeof(int))) int{n + 1};
        cout << "ptr: " << *ptr << " @ " << ptr << (mem.wrapAround() ? " (wrapped)" : "") << endl;
        ++n;
    }

}

int main() {
//    usingMonotonic();
    usingCircular();
    return 0;
}
