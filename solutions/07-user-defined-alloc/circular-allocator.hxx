#pragma once

namespace ribomation {
    using byte = unsigned char;

    template<unsigned CAPACITY = 1024>
    class CircularAllocator {
        byte storage[CAPACITY]{};
        byte* lastAddr = storage;
        bool wasWrapped = false;

    public:
        byte* allocate(unsigned numBytes) {
            wasWrapped = false;
            auto addr = lastAddr;
            lastAddr += numBytes;

            if (lastAddr > (storage + CAPACITY)) {
                lastAddr   = storage;
                addr       = lastAddr;
                wasWrapped = true;
                lastAddr += numBytes;
            }

            return addr;
        }

        [[nodiscard]] bool wrapAround() const {
            return wasWrapped;
        }
    };
}
