#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

// run as:
// valgrind --leak-check=full --leak-resolution=med --track-origins=yes ./memory-leak yes
// valgrind --leak-check=full --leak-resolution=med --track-origins=yes ./memory-leak no

int main(int argc, char** argv) {
    bool leak = true;
    if (argc > 1) {
        leak = std::string{argv[1]} != "no"s;
    }
    cout << "leak: " << (leak ? "YES" : "NO") << "\n";

    auto N = 10;
    void* ptr[N];
    for (auto k = 0; k < N; ++k) ptr[k] = malloc(1024);
    if (leak) return 1;

    for (auto k = 0; k < N; ++k) free(ptr[k]);
    return 0;
}
