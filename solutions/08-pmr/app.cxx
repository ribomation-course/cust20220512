#include <string>
#include <set>
#include <memory_resource>
#include <string_view>
#include <algorithm>
#include <cctype>
#include "no-heap.hxx"

using std::data;
using std::size;
using std::begin;
using std::end;
using std::string_view;
using std::uninitialized_fill;
using std::count;

using std::pmr::string;
using std::pmr::set;

using std::pmr::null_memory_resource;
using std::pmr::monotonic_buffer_resource;
using std::pmr::unsynchronized_pool_resource;
using std::pmr::set_default_resource;


int main() {
    char storage[2'000];
    uninitialized_fill(begin(storage), end(storage), '#');
    auto upstream = null_memory_resource();
    auto buffer   = monotonic_buffer_resource{data(storage), size(storage), upstream};
    auto memory   = unsynchronized_pool_resource{&buffer};
    set_default_resource(&memory);

    {
        auto words = set<string>{};
        words.emplace("CARIN");
        words.emplace("EVA");
        words.emplace("ANNA");
        words.emplace("DORIS");
        words.emplace("BERIT");
        words.emplace("FRIDA");

        printf("words: ");
        for (auto const& w : words) printf("%s ", w.c_str());
        printf("\n");
    }

    auto unusedBytes = count(begin(storage), end(storage), '#');
    printf("used: %ld bytes, unused: %ld bytes, total: %ld bytes\n",
           (size(storage) - unusedBytes), unusedBytes, size(storage));

    auto      sv = string_view{data(storage), size(storage)};
    auto idx = sv.find_last_not_of('#');
    printf("last used byte index: %ld\n", idx);

    {
        auto bytes = 0U;
        for (auto b : sv) {
            printf("%c", b == '#' ? '#' : (isalpha(b) ? b : '.'));
            if (++bytes % 100 == 0) printf("\n");
        }
        printf("\n");
    }
    ensure_no_heap();
    return 0;
}
