cmake_minimum_required(VERSION 3.16)
project(05_containers)

set(CMAKE_CXX_STANDARD 17)

add_executable(allocation-trace
        trace-alloc.hxx trace-alloc.cxx
        allocation-trace.cxx)
target_compile_options(allocation-trace PRIVATE -Wall -Wextra -Werror -Wfatal-errors)


