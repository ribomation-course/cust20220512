#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
using namespace std;

template<typename T = int>
class MessageQueue {
    mutex              lck;
    condition_variable notEmpty;
    queue<T>           inbox;
public:
    T get() {
        unique_lock<mutex> g{lck};
        notEmpty.wait(g, [this] { return !inbox.empty(); });
        T x = inbox.front();
        inbox.pop();
        return x;
    }
    void put(T x) {
        unique_lock<mutex> g{lck};
        inbox.push(x);
        notEmpty.notify_all();
    }
};

int main(int argc, char** argv) {
    auto N = argc == 1 ? 10 : stoi(argv[1]);
    auto q = MessageQueue<>{};

    auto   consumer = [&q]() {
        for (int msg; (msg = q.get()) > 0;) {
            cout << "[consumer] " << msg << "\n";
        }
    };
    thread c{consumer};

    for (int msg = 1; msg <= N; ++msg) q.put(msg);
    q.put(-1);
    c.join();
}
