cmake_minimum_required(VERSION 3.16)
project(09_threads)

set(CMAKE_CXX_STANDARD 17)

add_executable(threads app.cxx)
target_compile_options(threads PRIVATE -Wall -Wextra -Werror -Wfatal-errors )
target_link_libraries(threads PRIVATE -pthread)

