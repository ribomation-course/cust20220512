#include <iostream>
using std::cout;

void func(char ch, short s, int i, long l, long double ld) {
    cout << "[func] ch=" << ch
         << ", s=" << s
         << ", i=" << i
         << ", l=" << l
         << ", ld=" << ld
         << "\n";
}

int main() {
    cout << "[main] enter\n";
    func('A', 42, 1'000, 1'000'000, 3.1415 * 1E10);
    cout << "[main] exit\n";
}
