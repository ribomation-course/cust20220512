#include <iostream>

using namespace std;

int         bss_var;
int         data_var = 42;
char        a        = 'A';
short       b        = 20;
int         c        = 30;
long        d        = 40;
float       e        = 50.;
double      f        = 60.;
long double g        = 70.;

int main() {
    cout << "bss =" << bss_var << "\n";
    cout << "data=" << data_var << "\n";
    cout << a << ", " << b << ", " << c << ", " << d << ", " << e << ", " << f << ", " << g << "\n";
}
