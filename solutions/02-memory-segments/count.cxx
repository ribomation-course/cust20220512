#include <iostream>
#include <string>
#include <tuple>
#include <cctype>
#include "memory-mapped-file.hxx"

using namespace std;
using namespace ribomation;

auto count(MemoryMappedFile& file) -> tuple<unsigned, unsigned, unsigned> {
    auto lines = 0U;
    auto words = 0U;
    auto chars = file.size();

    auto buf = file.data();
    auto N   = file.size();

    for (auto k = 0UL; k < N; ++k) {
        if (buf[k] == '\n') ++lines;
        else if (isalpha(buf[k])) {
            ++words;
            while (isalpha(buf[++k]));
            --k;
            if (buf[k] == '\n') ++lines;
        }
    }

    return {lines, words, chars};
}

int main(int argc, char** argv) {
    auto filename = "../count.cxx"s;
    if (argc > 1) {
        filename = argv[1];
    }

    auto f = MemoryMappedFile{filename};
    cout << "loaded " << filename << ", " << f.size() << " bytes\n";

    auto[lines, words, chars] = count(f);
    cout << lines << "\t" << words << "\t" << chars << "\n";
    
    //new int[42];
}
