#include <iostream>
#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
using namespace std;

int main() {
    auto const N    = 1'000'000L;
    auto const size = N * sizeof(int);
    auto       fd   = open("./segment.bin", O_RDWR | O_CREAT | O_TRUNC, 0744);
    if (fd < 0) {
        throw runtime_error{"open failed: "s + strerror(errno)};
    }
    if (ftruncate(fd, static_cast<off_t>(size)) < 0) {
        throw runtime_error{"ftruncate failed: "s + strerror(errno)};
    }

    auto segment = mmap(0, size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
    if (segment == MAP_FAILED) {
        throw runtime_error{"mmap failed: "s + strerror(errno)};
    }

    int* array = static_cast<int*>(segment);
    for (auto k = 0; k < N; ++k) array[k] = k + 1;

    auto      sum = 0L;
    for (auto k   = 0; k < N; ++k) sum += array[k];

    cout << "sum: " << sum << "\n";
    cout << "SUM: " << (N * (N + 1) / 2) << "\n";

    if (munmap(segment, size) < 0) {
        throw runtime_error{"munmap failed: "s + strerror(errno)};
    }
}
