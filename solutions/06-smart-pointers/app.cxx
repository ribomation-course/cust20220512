#include <iostream>
#include <memory>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

auto func1(unique_ptr<Person> q) -> unique_ptr<Person> {
    q->incrAge();
    cout << "[func1] q: " << *q << endl;
    return q;
}

auto func3(shared_ptr<Person> q) -> shared_ptr<Person> {
    q->incrAge();
    cout << "[func3] q: " << *q << endl;
    cout << "[func3] refcnt: " << q.use_count() << endl;
    return q;
}

auto func2(shared_ptr<Person> q) -> shared_ptr<Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << endl;
    cout << "[func2] refcnt: " << q.use_count() << endl;
    q = func3(q);
    return q;
}

void use_unique_ptr() {
    cout << "--- usage of unique_ptr ---\n";
    auto anna = make_unique<Person>("Anna Conda"s, 42);
    cout << "[use_unique_ptr] anna: " << *anna << endl;
    auto ptr = move(anna);
    cout << "[use_unique_ptr] ptr: " << *ptr << endl;
    cout << "[use_unique_ptr] anna: " << anna.get() << endl;

    auto p = func1(move(ptr));
    cout << "[use_unique_ptr] p: " << *p << endl;
    cout << "[use_unique_ptr] anna: " << anna.get() << endl;
    cout << "[use_unique_ptr] ptr: " << ptr.get() << endl;

    ptr = make_unique<Person>("Per Silja", 37);
    cout << "[use_unique_ptr] ptr: " << *ptr << endl;

    p = make_unique<Person>("Sham Poo", 32);
    cout << "[use_unique_ptr] p: " << *p << endl;
}

void use_shared_ptr() {
    cout << "--- usage of shared_ptr ---\n";
    auto justin = make_shared<Person>("Justin Time"s, 37);
    cout << "[use_shared_ptr] justin: " << *justin << endl;
    cout << "[use_shared_ptr] refcnt: " << justin.use_count() << endl;
    {
        auto p = func2(justin);
        cout << "[use_shared_ptr] p: " << *p << endl;
        cout << "[use_shared_ptr] refcnt: " << p.use_count() << endl;
    }
    cout << "[use_shared_ptr] justin: " << *justin << endl;
    cout << "[use_shared_ptr] refcnt: " << justin.use_count() << endl;
}

int main() {
    cout << "# persons: " << Person::getCount() << endl;

    use_unique_ptr();
    cout << "# persons: " << Person::getCount() << endl;

    use_shared_ptr();
    cout << "# persons: " << Person::getCount() << endl;

    return 0;
}