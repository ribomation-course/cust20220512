# Homework 2

## Task Description

Compute word frequencies for a (large) text file and print out the N most frequent non-tiny words and their
corresponding count, not using the system heap at all.

---
## Update 2021-11-23

Skip overloading the `new` operator to throw an exception.
It turns out that `std::filesystem::file_size`, `std::ifstream`, `std::exception` all are creating `std::string`
objects, which are using `::operator new`.
If you *do* want to disable the system heap, read more below.

---

## Tips & Hints

* ~~Start by overloading the global `new` operators to throw an exception~~
  * ~~This is to catch any unforeseen usage of the system heap~~
* Create a sufficiently large memory segment, that has room for the whole text file plus your data-structure to keep track of the word frequencies
* Represent a word in your data-structure using `std::string_view`
  * That means it points to a span of chars within the file segment
  * https://en.cppreference.com/w/cpp/string/basic_string_view
* Implement an object pool allocator with run-time defined capacity, for your own linked data-structure
* Choose a data-structure you are comfortable with, that means choose linked-list or a binary-tree, or something else
* Just ensure it is fully allocated in the memory pool
* Get the size of the input file using std::filesystem::file_size
* Calculate the total size of the mmap:ed memory segment and create it
* Load the file into the file portion of the memory segment
* Scan the file portion, word by word (sequence of letters)
  * For each new word, create a std::string_view instance and set the count to 1
  * For an existing word, just increment its count
* In order to retrieve the N most frequent pairs, you need to provide for a linear sequence of the pairs, such as an array or similar
* Which can be sorted by any of
  * https://en.cppreference.com/w/cpp/algorithm/sort
  * https://en.cppreference.com/w/cpp/algorithm/partial_sort

### How to load a file into a memory segment

    namespace fs = std::filesystem;

    auto fileName = ...
    auto fileSize = fs::file_size(fileName);
    
    auto mem = MemorySegment{...};
    auto file = ifstream{fileName};
    if (!file) throw ...
    file.read(mem.data(), fileSize);


## Way of Working

* Pair up with a colleague and collaborate via teams and perhaps a shared git repo. Both GitLab and GitHub provide free repos, if you need it
* Ensure you have a decent application program that allocates and deallocates objects from your memory segment
* You can, for example, use a random number generator to decide whether to allocate or deallocate
    * https://en.cppreference.com/w/cpp/numeric/random
* Don't forget to test for exhausted memory pool 

## Extra task - _if you have the time and desire_
* Re-implement the solution now using PMR and corresponding `std::pmr` data-types, such as 
`std::unordered_map` and `std::vector`.
* Use shared memory and let a second process do the sorting and printing
  * You need to find a way to let this one wait until it can proceed

---
## Not using the system heap
If you *do* want to disable the system heap, here is how to achieve it:

## Remove certain std++ includes
Remove the following includes, if you have them:
* `iostream`
* `fstream`
* `string`
* `stdexcept`
* `filesystem`

### Print-outs
Use `printf` and `fprintf` for print-outs.

    printf("loaded %s, %ld chars\n", fileName, fileSize);

Include the following:
* `cstdlib`
* `cstdio`

### Errors
In case of an error; print to `stderr` and perform `exit(1)`.

    fprintf(stderr, "XXX(2) failed: %s (%d)\n", strerror(errno), errno);
    exit(1);

Include the following:
* `cstring`
* `cerrno`

### File size
Here we are using the system-call `stat` to read meta-data of a file
and the grab the file size property `st_size`.

    auto fileSize = 0UL;
    struct stat data{};
    auto rc = stat(filename, &data);
    if (rc == 0) fileSize = static_cast<unsigned long>(data.st_size);

Include the following:
* `sys/stat.h`

### Loading a file
Here we are using the system-calls `open` and `read` to load the file content
into the memory segment we created when we know the size of the `mmap` segment.

    auto fd = open(fileName, O_RDONLY);
    if (fd < 0) {...}
    auto fileBegin = memory.allocate(fileSize);
    auto rc = read(fd, fileBegin, fileSize);
    if (rc < 0) {...}

Include the following:
* `unistd.h`
* `fcntl.h`

