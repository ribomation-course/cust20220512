#include <string_view>
#include <algorithm>
#include <tuple>
#include <chrono>

#include <cstdlib>
#include <cstdio>
#include <cerrno>
#include <cstring>

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "word-freq.hxx"
#include "lib/memory-segment.hxx"
#include "lib/scan-word.hxx"
#include "lib/no-heap.hxx"

using namespace std;
using namespace std::chrono;
using namespace ribomation::memory;
using namespace ribomation::app;

auto parseArgs(int argc, char** argv) -> std::tuple<char*, unsigned, unsigned>;
auto load(const char* fileName, unsigned long fileSize, MemorySegment& memory) -> string_view;
auto aggregate(string_view& content, MemorySegment& memory, unsigned minWordSize) -> tuple<WordFreq*, unsigned>;
auto file_size(const char* filename) -> size_t;


int main(int argc, char** argv) {
    auto startTime = high_resolution_clock::now();

    auto[fileName, maxNumWords, minWordSize] = parseArgs(argc, argv);
    auto fileSize  = file_size(fileName);
    if (fileSize <= 0) {
        fprintf(stderr, "file '%s' not found\n", fileName);
        exit(1);
    }

    auto memorySize = 3 * fileSize;
    auto memory     = MemorySegment{memorySize};

    auto content = load(fileName, fileSize, memory);
    printf("Loaded %s: %ld chars\n", fileName, content.size());

    auto[wfArr, numWords] = aggregate(content, memory, minWordSize);
    printf("Parsed %d words\n", numWords);

    partial_sort(&wfArr[0], &wfArr[maxNumWords], &wfArr[numWords]);
    for_each_n(&wfArr[0], maxNumWords, [](const WordFreq& wf) {
       printf("%.*s: %d\n", static_cast<int>(wf.word.size()), wf.word.data(), wf.count);
    });

    auto endTime     = high_resolution_clock::now();
    auto elapsedTime = duration_cast<seconds>(endTime - startTime).count();
    printf("Elapsed %ld seconds\n", elapsedTime);

    ensure_no_heap();
}

auto parseArgs(int argc, char** argv) -> std::tuple<char*, unsigned, unsigned> {
    static char fileName_default[] = "../files/shakespeare.txt";
    char* fileName = fileName_default;
    auto maxNumWords = 100U;
    auto minWordSize = 5U;

    auto EQ = [](const char* lhs, const char* rhs) {
        return ::strcmp(lhs, rhs) == 0;
    };

    for (int k = 1; k < argc; ++k) {
        char* arg = argv[k];
        if (EQ(arg, "--file")) {
            fileName = argv[++k];
        } else if (EQ(arg, "--max")) {
            maxNumWords = atoi(argv[++k]);
        } else if (EQ(arg, "--min")) {
            minWordSize = atoi(argv[++k]);
        } else {
            fprintf(stderr, "usage: %s [--file <filename>] [---max <number>] [---min <number>]\\n", argv[0]);
            exit(1);
        }
    }

    return {fileName, maxNumWords, minWordSize};
}

auto file_size(const char* filename) -> size_t {
    struct stat data{};
    auto        rc = stat(filename, &data);
    if (rc == 0) return static_cast<size_t>(data.st_size);
    return 0UL;
}

auto load(const char* fileName, unsigned long fileSize, MemorySegment& memory) -> string_view {
    auto fd = open(fileName, O_RDONLY);
    if (fd < 0) {
        fprintf(stderr, "open(2) failed [%s]: %s (%d)\n", fileName, strerror(errno), errno);
        exit(1);
    }

    auto fileBegin = memory.allocate(fileSize);
    auto rc        = read(fd, fileBegin, fileSize);
    if (rc < 0) {
        fprintf(stderr, "read(2) failed: %s (%d)\n", strerror(errno), errno);
        exit(2);
    }
    return {fileBegin, fileSize};
}

auto aggregate(string_view& content, MemorySegment& memory, unsigned minWordSize) -> tuple<WordFreq*, unsigned> {
    WordFreq* wfArr = nullptr;
    auto numWords = 0U;

    for (auto word = nextWord(content); word; word = nextWord(content)) {
        if (word->size() < static_cast<unsigned long>(minWordSize)) {
            continue;
        }

        if (wfArr == nullptr) {
            wfArr    = new(memory.allocate(sizeof(WordFreq))) WordFreq{*word};
            numWords = 1;
        } else {
            auto maybe = WordFreq{*word};
            auto it    = find(&wfArr[0], &wfArr[numWords], maybe);
            if (it == &wfArr[numWords]) { //not found
                new(memory.allocate(sizeof(WordFreq))) WordFreq{*word};
                ++numWords;
            } else {
                it->count++;
            }
        }
    }

    return {wfArr, numWords};
}
