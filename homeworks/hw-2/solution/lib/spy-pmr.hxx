#pragma once

#include <memory_resource>

namespace ribomation::memory {
    using std::pmr::memory_resource;

    struct spy_pmr : memory_resource {
        explicit spy_pmr(memory_resource& upstream_, unsigned long size) : upstream(upstream_), capacity{size} {
            printf("[spy-pmr] created buffer %ld bytes\n", capacity);
        }

        void stats() const {
            printf("[spy-pmr] total capacity: %ld bytes\n", capacity);
            printf("[spy-pmr] max allocated : %ld bytes (%.1f %%)\n",
                   maxAllocatedBytes, 100.0 * maxAllocatedBytes / capacity);
            printf("[spy-pmr] curr allocated: %ld bytes (%.1f %%)\n",
                   allocatedBytes, 100.0 * allocatedBytes / capacity);
            printf("[spy-pmr] available     : %ld bytes (%.1f %%)\n",
                   capacity - allocatedBytes, 100.0 * (capacity - allocatedBytes) / capacity);

        }

    protected:
        void* do_allocate(size_t bytes, size_t alignment) override {
            auto blk = upstream.allocate(bytes, alignment);
            allocatedBytes += bytes;
            maxAllocatedBytes += bytes;
            return blk;
        }

        void do_deallocate(void* p, size_t bytes, size_t alignment) override {
            upstream.deallocate(p, bytes, alignment);
            allocatedBytes -= bytes;
        }

        [[nodiscard]]
        bool do_is_equal(const memory_resource& other) const noexcept override {
            return upstream.is_equal(other);
        }

    private:
        memory_resource& upstream;
        unsigned long capacity          = 0;
        unsigned long allocatedBytes    = 0;
        unsigned long maxAllocatedBytes = 0;
    };

}