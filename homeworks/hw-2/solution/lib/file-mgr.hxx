#pragma once

#include <cstdio>

namespace ribomation::io {

    struct FileMgr {
        const char* name;
        FILE      * file;

        explicit FileMgr(const char* name_) : name{name_}, file{fopen(name_, "w")} {
            if (file == nullptr) {
                fprintf(stderr, "cannot open %s\n", name);
                exit(1);
            }
        }

        operator FILE*() {
            return file;
        }

        ~FileMgr() {
            fclose(file);
            printf("Written %s\n", name);
        }
    };

}
