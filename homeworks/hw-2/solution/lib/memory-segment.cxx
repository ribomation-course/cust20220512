#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <sys/mman.h>
#include "memory-segment.hxx"


namespace ribomation::memory {

    MemorySegment::MemorySegment(size_t size) {
        storageSize  = size;
        storageBegin = mmap(nullptr, storageSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        if (storageBegin == MAP_FAILED) {
            fprintf(stderr, "mmap(2) failed: %s (%d)\n", strerror(errno), errno);
            fprintf(stderr, "  size=%ld\n", size);
            exit(3);
        }
        storageEnd  = reinterpret_cast<char*>(storageBegin) + storageSize;
        nextAddress = storageBegin;
    }

    MemorySegment::~MemorySegment() {
        munmap(storageBegin, storageSize);
    }

    char* MemorySegment::allocate(unsigned int numBytes) {
        auto addr = nextAddress;
        nextAddress = reinterpret_cast<char*>(nextAddress) + numBytes;
        return static_cast<char*>(addr);
    }
}

