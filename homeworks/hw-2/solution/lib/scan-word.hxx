#pragma once

#include <optional>
#include <string_view>

namespace ribomation::app {
    using std::optional;
    using std::string_view;

    inline auto nextWord(string_view& content) -> std::optional<string_view> {
        static char letters[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        auto begin = content.find_first_of(letters);
        if (begin == string_view::npos) {
            return {};
        }

        auto end = content.find_first_not_of(letters, begin);
        if (end == string_view::npos) {
            return {};
        }

        auto word = content.substr(begin, end - begin);
        content.remove_prefix(std::min(end + 1, content.size()));

        return word;
    }

}
