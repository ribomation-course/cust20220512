#pragma once

#include <string>
#include "memory-segment.hxx"

namespace ribomation::demo {
    using namespace std;
    using namespace ribomation::memory;

    class Account {
        static constexpr int  ACCNO_SIZE = 16;
        static constexpr char PAD_CHAR   = '#';

        char accno[ACCNO_SIZE]{};
        int  balance{};

    public:
        static MemorySegment pool;
        static bool          verbose;

        Account();
        Account(const string& accno_, int balance_);
        ~Account();
        Account(Account const&) = delete;
        Account& operator=(Account const&) = delete;

        [[nodiscard]] int getBalance() const { return balance; }
        [[maybe_unused]] void updateBalance(int amount) { balance += amount; }
        [[nodiscard]] string getAccno() const;
        void setAccno(const string& s);

        void* operator new(size_t) {
            return pool.allocate();
        }

        void operator delete(void* ptr) {
            pool.deallocate(ptr);
        }
    };

    inline auto operator<<(ostream& os, const Account& a) -> ostream& {
        return os << "Account{" << a.getAccno() << ", SEK " << a.getBalance() << "}";
    }

    string nextAccno();
    int nextBalance();

}