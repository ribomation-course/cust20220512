#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include "account.hxx"

using namespace std;
using namespace ribomation::demo;

void use_case_1(unsigned N) {
    cout << "-- use-case 1: dummy pointers in a vector --\n";
    auto accounts = vector<Account*>{};
    accounts.reserve(N);

    for (auto k = 0U; k < N; ++k) {
        auto acc = new Account{nextAccno(), nextBalance()};
        accounts.push_back(acc);
    }
    if (Account::verbose) for (auto a: accounts) cout << *a << "\n";
    for (auto a: accounts) delete a;
}

void use_case_2(unsigned N) {
    cout << "-- use-case 2: smart pointers in a vector --\n";
    auto accounts = vector<unique_ptr<Account>>{};
    accounts.reserve(N);

    for (auto k = 0U; k < N; ++k) {
        auto acc = make_unique<Account>(nextAccno(), nextBalance());
        accounts.push_back(std::move(acc));
    }
    if (Account::verbose) for (auto const& a: accounts) cout << *a << "\n";
}
void use_case_2b(unsigned N) {
    cout << "-- use-case 2b: smart pointers in a vector, using emplace_back --\n";
    auto accounts = vector<unique_ptr<Account>>{};
    accounts.reserve(N);

    for (auto k = 0U; k < N; ++k) {
        accounts.emplace_back(make_unique<Account>(nextAccno(), nextBalance()));
    }
    if (Account::verbose) for (auto const& a: accounts) cout << *a << "\n";
}


int main(int argc, char** argv) {
    auto const N = argc == 1 ? 10U : stoi(argv[1]);
    Account::pool.setup(N);
    Account::verbose = (N <= 100U);
    cout<<"N="<<N<<"\n";
    cout<<"Account::verbose="<<std::boolalpha<<Account::verbose<<"\n";

    use_case_1(N);
    use_case_2(N);
    use_case_2b(N);
}

