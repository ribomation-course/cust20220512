#include <iostream>
#include <stdexcept>
#include <cerrno>
#include <cstring>
#include <algorithm>
#include <iterator>
#include <sys/mman.h>
#include "memory-segment.hxx"

namespace ribomation::memory {
    using namespace std;

    void MemorySegment::setup(unsigned capacity) {
        memSegmentSize = capacity * objectSize;

        memSegmentStart = mmap(0, memSegmentSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        if (memSegmentStart == MAP_FAILED) {
            throw runtime_error{"mmap failed: "s + strerror(errno)};
        }

        inUse.reserve(capacity);
        fill_n(back_inserter(inUse), capacity, false);
        cerr << "mem-segment initialized: " << inUse.size() << " slots (" << memSegmentSize << " bytes)\n";
    }

    void MemorySegment::shutdown() {
        if (munmap(memSegmentStart, memSegmentSize) < 0) {
            throw runtime_error{"munmap failed: "s + strerror(errno)};
        }
        cerr << "mem-segment disposed: " << inUse.size() << " slots (" << memSegmentSize << " bytes)\n";
    }

    void* MemorySegment::allocate() {
        if (memSegmentStart == nullptr) {
            throw runtime_error{"memory segment not initialized"s};
        }

        //find the first available slot, mark it in-use and return its address
        auto availPos = find(inUse.begin() + nextIndex, inUse.end(), false);
        if (availPos == inUse.end()) {
            nextIndex = 0;
            availPos  = find(inUse.begin(), inUse.end(), false);
        }
        if (availPos == inUse.end()) {
            throw overflow_error{"memory segment exhausted"s};
        }

        *availPos = true;
        auto availIndex = distance(inUse.begin(), availPos);
        nextIndex = availIndex;

        auto start = reinterpret_cast<unsigned long>(memSegmentStart);
        return reinterpret_cast<void*>(start + availIndex * objectSize);
    }

    void MemorySegment::deallocate(void* ptr) {
        //transform the address back to an index, mark it available
        auto end   = reinterpret_cast<unsigned long>(ptr);
        auto start = reinterpret_cast<unsigned long>(memSegmentStart);
        auto index = (end - start) / objectSize;
        inUse[index] = false;
    }

}

