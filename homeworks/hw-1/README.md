# Homework 1

## Task Description
Implement a data-type having its own overloaded pair of new / delete, where object allocation should 
go to a `mmap`:ed memory segment

# Tips & Hints

* Ensure your data-type do not contain any external parts, i.e., no pointer to outside the memory segment 
  * Use scalar types, such as int, floats, …
  * You can use a char array, if you implement pack/unpack methods
* Treat the memory segment as a large array of your data-type and keep track of which elements are available and which are in-use
  * The simplest way to do that is a boolean within the data-type, but you can also use a separate std::vector<bool> or similar
*  Implement two static methods, were the first creates the memory segment and the second takes it down
*  Throw an exception if the memory pool is exhausted

## Sample Data-Type

    class Account {
      int   balance = 0;
      char  accno[16];
      static void*   memSegmentStart;
      static size_t  memSegmentSize;
    public:
      Account() {}
      void   setAccno(string s) {s.copy(accno, 16);}
      string getAccno() const {…}
      void   updateBalance(int amount) {balance += amount;}
      int    getBalance() const {…}
    
      void* operator new(size_t n) { 
        //find the first available slot, mark it in-use and return its address
      }
      void  operator delete(void* ptr) { 
        //transform the address back to an index, mark it available
      }
    
      static void init(int numSlots) {…}
      static void dispose() {…}
    }

## How to create a memory segment

    auto const size    = ...;
    auto       segment = mmap(0, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (segment == MAP_FAILED) {
        throw runtime_error{"mmap failed: "s + strerror(errno)};
    }


# Way of Working

* Pair up with a colleague and collaborate via teams and perhaps a shared git repo. Both GitLab and GitHub provide free repos, if you need it
* Ensure you have a decent application program that allocates and deallocates objects from your memory segment
* You can, for example, use a random number generator to decide whether to allocate or deallocate
  * https://en.cppreference.com/w/cpp/numeric/random
* Don't forget to test for exhausted memory pool 

# Extra Task: _If you have the time and desire_

* Let your data-type be part of a linked data-structure, such as a single-linked or double-linked list, or a linked binary tree

Keep in mind, that you need two data-types, one for the link objects and one for the owning ("_surrounding_") object, i.e., `Node` vs. `List`.

# Solution

* [app.cxx](./solution/app.cxx)
* [account.hxx](./solution/account.hxx)
* [account.cxx](./solution/account.cxx)
* [memory-segment.hxx](./solution/memory-segment.hxx)
* [memory-segment.cxx](./solution/memory-segment.cxx)

## Usage

    $ cd path/to/solution/
    $ cmake -S . -B bld
    $ cmake --build bld
    $ ./bld/hw1

 ### Change the number of accounts

    $ ./bld/hw1 25

