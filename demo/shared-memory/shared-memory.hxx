#pragma once

#include <string>
#include <sstream>
#include <stdexcept>
#include <iterator>

#include <cstring>
#include <cerrno>
#include <cmath>

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/wait.h>

namespace ribomation::shm {
    using namespace std;
    using namespace std::literals;

    struct SharedMemory {
        SharedMemory(size_t shmSize, const string& shmName = "/cxx-shm", void* startAddr = 0) {
            name  = (shmName.at(0) == '/') ? shmName : "/"s + shmName;
            size  = alignSize(shmSize);
            begin = mkShm(startAddr);
            end   = reinterpret_cast<char*>(begin) + size;
            next  = begin;
        }

        ~SharedMemory() {
            munmap(begin, size);
            shm_unlink(name.c_str());
        }

        void* allocate(size_t numBytes) {
            void* addr = next;
            next = reinterpret_cast<char*>(next) + numBytes;
            if (end <= next) throw overflow_error{"SHM"s};
            return addr;
        }

        template<typename T>
        T* allocate(unsigned numElems = 1) {
            return reinterpret_cast<T*>(allocate(numElems * sizeof(T)));
        }

        void deallocate(void* addr) { /*do nothing*/ }

        SharedMemory() = delete;
        SharedMemory(const SharedMemory&) = delete;
        SharedMemory& operator =(const SharedMemory&) = delete;

    private:
        size_t size;
        string name;
        void* begin;
        void* end;
        void* next;

        void* mkShm(void* startAddr) {
            auto shmFd = shm_open(name.c_str(), O_CREAT | O_TRUNC | O_RDWR, 0600);
            if (shmFd == -1) fail("shm_open");
            if (ftruncate(shmFd, size) == -1) fail("ftruncate");
            auto shm = mmap(startAddr, size, PROT_READ | PROT_WRITE, MAP_SHARED, shmFd, 0);
            if (shm == MAP_FAILED) fail("mmap");
            close(shmFd);
            return shm;
        }

        static auto alignSize(size_t size) -> size_t {
            auto pageSize = static_cast<double>(getpagesize());
            return static_cast<size_t>(ceil(size / pageSize) * pageSize);
        }

        static void fail(const string& funcName) {
            auto buf = ostringstream{};
            buf << funcName << "(): " << strerror(errno);
            throw runtime_error{buf.str()};
        }
    };


}
