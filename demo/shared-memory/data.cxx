#include <iostream>
#include "data.hxx"

namespace ribomation::data {

    auto fibonacci(int n) -> unsigned long {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return fibonacci(n - 2) + fibonacci(n - 1);
    }

    auto operator <<(ostream& os, const Data& d) -> ostream& {
        return os << "f(" << d.arg << ") = " << d.result;
    }

    Data::Data() { cout << "Data() @ " << this << "\n"; }
    Data::~Data() { cout << "~Data() @ " << this << "\n"; }


}
