#include <iostream>
#include <string>
#include <stdexcept>
#include <chrono>

#include <cstring>
#include <cerrno>

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/wait.h>

using namespace std;
using namespace std::literals;
using namespace std::chrono;

struct Data {
    int           arg;
    unsigned long result;
};

auto fibonacci(int n) -> unsigned long {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

auto operator <<(ostream& os, const Data& d) -> ostream& {
    return os << "f(" << d.arg << ") = " << d.result;
}

auto computeShmSize(int N) -> unsigned long {
    auto shmSize  = N * sizeof(Data);
    auto pageSize = getpagesize();
    if (shmSize <= static_cast<size_t>(pageSize)) {
        return pageSize;
    } else {
        return (1 + shmSize / pageSize) * pageSize;
    }
}

void* createShm(string name, int N) {
    auto shmFd = shm_open(name.c_str(), O_CREAT | O_TRUNC | O_RDWR, 0600);
    if (shmFd == -1)
        throw invalid_argument{"cannot create shm file: "s + strerror(errno)};

    auto shmSize = computeShmSize(N);
    if (ftruncate(shmFd, shmSize) == -1)
        throw invalid_argument{"cannot inflate shm file: "s + strerror(errno)};

    auto shm = mmap(0, shmSize, PROT_READ | PROT_WRITE, MAP_SHARED, shmFd, 0);
    if (shm == MAP_FAILED)
        throw invalid_argument{"cannot map shm segment: "s + strerror(errno)};
    close(shmFd);

    return shm;
}

void generator(Data* arr, int N) {
    cout << "[child] generating " << N << " results...\n";
    auto      startTime = high_resolution_clock::now();
    for (auto k         = 0; k < N; ++k) {
        arr[k].arg    = k + 1;
        arr[k].result = fibonacci(arr[k].arg);
    }
    auto      endTime   = high_resolution_clock::now();
    auto      elapsed   = duration_cast<milliseconds>(endTime - startTime).count();
    cout << "[child] elapsed " << elapsed / 1000.0 << " seconds\n";
}

void printer(Data* arr, int N) {
    cout << "[parent] printing " << N << " results\n";
    for (auto k = 0; k < N; ++k) {
        cout << arr[k] << "\n";
    }
}

int main() {
    auto numValues = 42;
    auto shmName   = "/fib-storage"s;
    auto shm       = createShm(shmName, numValues);

    auto rc = fork();
    if (rc == -1)
        throw invalid_argument{"cannot create child process: "s + strerror(errno)};
    if (rc == 0) {
        generator(reinterpret_cast<Data*>(shm), numValues);
        exit(0);
    }

    auto status = 0;
    wait(&status);
    printer(reinterpret_cast<Data*>(shm), numValues);
    shm_unlink(shmName.c_str());

    return 0;
}


