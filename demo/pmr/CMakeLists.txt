cmake_minimum_required(VERSION 3.16)
project(pmr)

set(CMAKE_CXX_STANDARD 17)
set(NOWARN -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable -Wno-pointer-arith)
set(WARN -Wall -Wextra -Wfatal-errors ${NOWARN})

add_executable(using-monotonic-storage using-monotonic-storage.cxx)
target_compile_options(using-monotonic-storage PRIVATE ${WARN})


add_executable(understanding-sso understanding-sso.cxx)
target_compile_options(understanding-sso PRIVATE ${WARN})


add_executable(using-pool-storage no-heap.hxx using-pool-storage.cxx)
target_compile_options(using-pool-storage PRIVATE ${WARN})


add_executable(user-defined-type user-defined-type.cxx)
target_compile_options(user-defined-type PRIVATE ${WARN})


add_executable(using-cyclic-storage using-cyclic-storage.cxx)
target_compile_options(using-cyclic-storage PRIVATE ${WARN})


