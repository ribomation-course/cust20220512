#include <memory_resource>
#include <string>
#include <vector>
#include <cstdio>
#include <cstring>
#include "no-heap.hxx"

using std::data;
using std::size;
using std::to_string;

using std::pmr::monotonic_buffer_resource;
using std::pmr::polymorphic_allocator;
using std::pmr::null_memory_resource;

using std::pmr::vector;
using std::pmr::string;

class Person {
    string   name;
    unsigned age;
public:
    using allocator_type = polymorphic_allocator<char>;

    Person(const char* name, unsigned age, allocator_type alloc = {})
            : name{name, alloc}, age{age} {
        printf("Person(%s, %d) @ %p\n", name, age, this);
    }

    Person(const Person& that, allocator_type alloc)
            : name{that.name, alloc}, age{that.age} {
        printf("Person(const Person& %s, %d) @ %p\n", name.c_str(), age, this);
    }

    Person(Person&& that, allocator_type alloc)
            : name{move(that.name), alloc}, age{that.age} {
        printf("Person(Person&& %s, %d) @ %p\n", name.c_str(), age, this);
    }

    ~Person() {
        printf("~Person(%s, %d) @ %p\n", name.c_str(), age, this);
    }

    const string& getName() const { return name; }
    unsigned getAge() const { return age; }
};


struct spy_monotonic_buffer_resource : monotonic_buffer_resource {
    using super = monotonic_buffer_resource;

    spy_monotonic_buffer_resource(void* buffer, size_t bufferSize, memory_resource* upstream)
            : super(buffer, bufferSize, upstream) {
        printf("[buf] created, size=%ld\n", bufferSize);
    }

    void printStats(const char* msg) {
        printf("[buf] [%s] current: %ld bytes, max: %ld bytes\n", msg, curAllocBytes, maxAllocBytes);
    }

protected:
    void* do_allocate(size_t bytes, size_t alignment) override {
        printf("[buf] allocate(%ld bytes, align=%ld)\n", bytes, alignment);
        auto blk = super::do_allocate(bytes, alignment);
        curAllocBytes += bytes;
        maxAllocBytes += bytes;
        printf("[buf]  -> %p (cur=%ld, max=%ld)\n", blk, curAllocBytes, maxAllocBytes);
        return blk;
    }

    void do_deallocate(void* p, size_t bytes, size_t alignment) override {
        printf("[buf] deallocate(%p, %ld bytes, %ld)\n", p, bytes, alignment);
        super::do_deallocate(p, bytes, alignment);
        curAllocBytes -= bytes;
        printf("[buf]  -> (cur=%ld, max=%ld)\n", curAllocBytes, maxAllocBytes);
    }

    bool do_is_equal(const memory_resource& __other) const noexcept override {
        return super::do_is_equal(__other);
    }

private:
    long maxAllocBytes = 0;
    long curAllocBytes = 0;
};

template<size_t N>
constexpr size_t
strSize(const char (&)[N]) { return N; }


int main() {
    constexpr auto NAME    = "..Nisse Hult i Bagarmossen";
    constexpr auto NAME_SZ = strSize("..Nisse Hult i Bagarmossen");
    constexpr auto P       = sizeof(Person) + NAME_SZ;
    constexpr auto N       = 2;

    char           storage[N * P];
    printf("sizeof(Person)  = %ld bytes\n", sizeof(Person));
    printf("sizeof(name)    = %ld bytes\n", NAME_SZ);
    printf("sizeof(storage) = %ld bytes\n", sizeof(storage));

    auto upstream = null_memory_resource();
    auto memory   = spy_monotonic_buffer_resource{data(storage), size(storage), upstream};
    set_default_resource(&memory);
    {
        auto persons = vector<Person>{};
        persons.reserve(N);
        printf("----\n");
        for (auto k = 0; k < N; ++k) {
            char buf[64];
            strncpy(buf, NAME, sizeof(buf));
            buf[0] = '0' + k;
            auto age = 20U + k % 50;
            persons.emplace_back(buf, age);
        }
        for (auto const& p : persons)
            printf("* person(%s, %d)\n", p.getName().c_str(), p.getAge());
        printf("----\n");
    }
    memory.printStats("after block");
    try {
        auto p = new int{42};
        printf("unexpected: %p !!!\n", p);
    } catch (std::bad_alloc& x) {
        printf("Confirmed: No heap memory was used!\n");
    }
    return 0;
}


