#include <iostream>
#include <vector>
#include <memory_resource>
#include <cstdio>

namespace ribomation::memory {
    using std::pmr::memory_resource;

    struct cyclic_buffer_resource : memory_resource {
        cyclic_buffer_resource(void* storage, size_t size) noexcept
                : storage{storage},
                  storageEnd{ADD(storage, size)},
                  nextAddress{storage} {}

        ~cyclic_buffer_resource() override = default;
        cyclic_buffer_resource(const cyclic_buffer_resource&) = delete;
        cyclic_buffer_resource& operator =(const cyclic_buffer_resource&) = delete;

    protected:
        void* do_allocate(size_t bytes, size_t alignment) override {
            printf("do_allocate(%ld bytes)\n", bytes);
            if (ADD(nextAddress, bytes) >= storageEnd) {
                nextAddress = storage;
            }
            auto blockStart = nextAddress;
            nextAddress = ADD(nextAddress, bytes);
            printf("  -> index=[%ld, %ld] (%p)\n",
                    SUB(storage, blockStart),  SUB(storage, nextAddress)-1, blockStart);
            return blockStart;
        }

        void do_deallocate(void* __p, size_t __bytes, size_t __alignment) override {
            // nothing
        }

        bool do_is_equal(const memory_resource& that) const noexcept override {
            return this == &that;
        }

    private:
        void* const storage;
        void* const storageEnd;
        void* nextAddress;
        void* ADD(void* base, size_t offset) const {
            return static_cast<char*>(base) + offset;
        }
        long SUB(void* first, void* last) {
            return static_cast<char*>(last) - static_cast<char*>(first);
        }
    };
}

using std::data;
using std::size;
using std::uninitialized_fill_n;
using std::pmr::vector;
using ribomation::memory::cyclic_buffer_resource;

int main() {
    constexpr auto N = 42U;
    char           storage[N];
    uninitialized_fill_n(data(storage), size(storage), '.');

    auto memory = cyclic_buffer_resource{data(storage), size(storage)};
    auto chars  = vector<char>(&memory);
    for (auto ch = 'a'; ch <= 'z'; ++ch) chars.push_back(ch);
    storage[N - 1] = '\0';
    printf("storage[%s]", storage);

    return 0;
}


