#pragma once

#include <cstdio> //printf

namespace ribomation::data {

    struct Data {
        int    ival;
        double dval;

        Data(int ival, double dval) : ival(ival), dval(dval) {
            print("CREATE");
        }

        ~Data() {
            print("DESTROY");
        }

        void print(const char* prefix = "") {
            printf("%s Data{%d, %lf} @ %p\n", prefix, ival, dval, this);
        }
    };

}


