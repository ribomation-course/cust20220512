#pragma once

#include <cstddef>
#include <new>

auto operator new(size_t ) -> void* {
    throw std::bad_alloc{};
}

auto operator new[](size_t ) -> void* {
    throw std::bad_alloc{};
}

void noHeapCheck() {
    try {
        new int{42};
    } catch (...) {
        printf("No heap space was harmed in this production\n");
    }
}
