#pragma once

#include <memory>
#include <bitset>
#include <stdexcept>

#define interface struct
#define abstract 0
#define implements public

namespace ribomation::memory {
    using std::bitset;

    template<typename Type>
    interface BlockPool {
        virtual auto allocate() -> Type*   = abstract;
        virtual void deallocate(Type* ptr) = abstract;

        struct Deleter {
            BlockPool<Type>* pool;

            Deleter(BlockPool<Type>* pool_) : pool{pool_} {}

            void operator()(Type* ptr) {
                ptr->~Type();
                pool->deallocate(ptr);
            }
        };

        Deleter myDeleter{this};
    };


    template<typename Type, unsigned numBlocks>
    struct StaticBlockPool : implements BlockPool<Type> {
        auto allocate() -> Type* {
            if (full()) throw std::overflow_error{"pool full"};
            auto idx = nextIndex();
            usage.set(idx);
            return toAddress(idx);
        }

        void deallocate(Type* ptr) {
            if (isMember(ptr)) {
                auto idx = toIndex(ptr);
                usage.reset(idx);
            } else {
                throw std::out_of_range{"not from this pool"};
            }
        }

        unsigned allocated() const { return usage.count(); }
        unsigned available() const { return numBlocks - allocated(); }
        bool full() const { return available() == 0; }

        StaticBlockPool() = default;
        ~StaticBlockPool() = default;
        StaticBlockPool(const StaticBlockPool<Type, numBlocks>&) = delete;
        auto operator=(const StaticBlockPool<Type, numBlocks>&) -> StaticBlockPool<Type, numBlocks>& = delete;

    private:
        using byte = unsigned char;
        constexpr static size_t blockSize = sizeof(Type);
        constexpr static size_t storageSize = numBlocks * blockSize;
        bitset<numBlocks> usage{};
        byte storage[storageSize]{};

        unsigned nextIndex() const {
            unsigned index = 0;
            while (index < usage.size() && usage.test(index)) ++index;
            return index;
        }

        Type* toAddress(unsigned index) {
            return reinterpret_cast<Type*>(storage + index * blockSize);
        }

        unsigned toIndex(Type* ptr) const {
            auto const begin = reinterpret_cast<unsigned long>(storage);
            auto const addr  = reinterpret_cast<unsigned long>(ptr);
            auto const index = (addr - begin) / blockSize;
            return static_cast<unsigned>(index);
        }

        bool isMember(Type* ptr) const {
            auto addr  = reinterpret_cast<byte*>(ptr);
            auto begin = storage;
            auto end   = begin + storageSize;
            return (begin <= addr) && (addr < end);
        }
    };


    template<typename Type>
    using SmartPtr = std::unique_ptr<Type, typename BlockPool<Type>::Deleter>;

    template<typename Type, typename... ConstructorArgs>
    auto make_smart_ptr(BlockPool<Type>& pool, ConstructorArgs... args) -> SmartPtr<Type> {
        auto block = pool.allocate();
        auto obj   = new (block) Type{args...};
        return SmartPtr<Type>{obj, pool.myDeleter};
    }

}
