#include <sstream>
#include <memory>
#include <algorithm>
#include <cstring>
#include <cxxabi.h>
#include <execinfo.h>
#include "stack-trace.hxx"

using namespace std;
using namespace std::literals;

namespace ribomation {

    /**
     * Used by unique_ptr<T> to dispose malloc payloads
     */
    struct FreeDeleter {
        void operator ()(void* ptr) { free(ptr); }
    };

    /**
     * Returns a demangled C++ name from a linker symbol name
     * @param mangledName
     * @return demangled C++ name
     */
    auto demangle(const char* mangledName) -> string {
        if (mangledName == nullptr || strlen(mangledName) == 0) {
            return {};
        }

        auto resultStatus  = 0;
        auto demangledName = unique_ptr<char, FreeDeleter>{
                abi::__cxa_demangle(mangledName, nullptr, nullptr, &resultStatus)};
        switch (resultStatus) {
            case 0:
                return {demangledName.get()};
            case -1:
                throw runtime_error{"could not do malloc within abi::__cxa_demangle()"};
            case -2:
                return string{mangledName} + "()"s;
            case -3:
                throw invalid_argument{"invalid"s + mangledName};
            default:
                throw runtime_error{"unexpected error"};
        }
    }

    /**
     * Extracts the function name part of a backtrace info string
     * @param backtraceInfo
     * @return function name part of a backtrace
     */
    auto extractName(const char* backtraceInfo) -> string {
        if (backtraceInfo == nullptr || strlen(backtraceInfo) == 0) {
            return {};
        }

        auto       payload = string_view{backtraceInfo, strlen(backtraceInfo)};
        auto const LEFT    = "("sv;
        auto const PLUS    = "+"sv;

        auto start = payload.find(LEFT);
        if (start == string_view::npos) { return {}; }

        auto end = payload.find(PLUS, start + 1);
        if (end == string_view::npos) { return {}; }

        auto name = payload.substr(start + 1, end - start - 1);;
        return {name.data(), name.size()};
    }

    auto createBacktrace(unsigned frameOffset) -> vector<string> {
        auto stack = vector<string>{};

        const auto MAX_STACK = 1024U;
        void* stackTraceAddresses[MAX_STACK];
        unsigned  numFrames  = backtrace(stackTraceAddresses, MAX_STACK);
        auto      stackTrace = unique_ptr<char*, FreeDeleter>{backtrace_symbols(stackTraceAddresses, numFrames)};
        for (auto k          = frameOffset; k < numFrames; ++k) {
            auto stkInfo         = stackTrace.get()[k];
            auto functionMangled = extractName(stkInfo);
            auto function        = demangle(functionMangled.c_str());
            if (!function.empty()) {
                stack.push_back(function);
            }
        }

        return stack;
    }


    StackTrace::StackTrace(unsigned frameOffset)
            : callStack{createBacktrace(frameOffset)} {}

    ApplicationException::ApplicationException(const string& msg, unsigned int frameOffset)
            : runtime_error{msg},
              stackTrace{frameOffset} {}

    void ApplicationException::printStackTrace(ostream& os) const {
        auto frameNo = 0U;
        for (auto const& function : stackTrace.callStack) {
            os << ++frameNo << ") " << function << "\n";
        }
    }


    SystemException::SystemException(const string& msg)
            : ApplicationException{fmt(msg), 4},
              systemError{system_category().default_error_condition(errno)} {}


    string SystemException::fmt(const string& msg) {
        auto error = system_category().default_error_condition(errno);
        auto buf   = ostringstream{};
        buf << msg << ": " << error.message() << " (errno=" << error.value() << ")";
        return buf.str();
    }


    void registerSignal(int signo, sighandler_t handler) {
        struct sigaction action{};
        action.sa_handler = handler;
        action.sa_flags   = SA_NODEFER;  //Do not prevent the signal from being received from within its own signal handler. sigaction(2)
        if (sigaction(signo, &action, nullptr) != 0)
            throw invalid_argument{"Failed to register signal handler: "s + strerror(errno)};

        sigset_t signals{};
        sigaddset(&signals, signo);
        if (sigprocmask(SIG_UNBLOCK, &signals, nullptr) != 0)
            throw invalid_argument{"Failed to setup signal: "s + strerror(errno)};
    }

    auto operator <<(ostream& os, const ApplicationException& err) -> ostream& {
        os << "ApplicationException: " << err.what() << endl;
        err.printStackTrace(os);
        return os;
    }

    void crashHandler(int signo) {
        throw CrashException{signo};
    }

    CrashException::CrashException(int signo)
            : ApplicationException{strsignal(signo), 5},
              code{signo},
              message{strsignal(signo)} {}

    void CrashException::init(initializer_list<int> signoList) {
        for_each(signoList.begin(), signoList.end(), [](int signo) {
            registerSignal(signo, &crashHandler);
        });
    }


}
