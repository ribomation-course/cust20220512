#include "catch2-unit-testing.hxx"
#include "stack-trace.hxx"

using namespace std::literals;
using namespace ribomation;

TEST_CASE("demangle: passing nullptr, should return an empty string", "[demangle]") {
    REQUIRE(demangle(nullptr) == ""s);
}

TEST_CASE("demangle: passing an empty string, should return an empty string", "[demangle]") {
    REQUIRE(demangle("") == ""s);
}

TEST_CASE("passing a non-C++ name, should return it unaffected", "[demangle]") {
    REQUIRE(demangle("strcpy") == "strcpy()"s);
}

TEST_CASE("passing a C++ name, should return its clear-text name", "[demangle]") {
    REQUIRE(demangle("_ZN10ribomation5log_2Ed") == "ribomation::log_2(double)"s);
}

