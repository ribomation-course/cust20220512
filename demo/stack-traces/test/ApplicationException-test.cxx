#include "catch2-unit-testing.hxx"
#include "stack-trace.hxx"
#include <sstream>

using namespace std::literals;
using namespace ribomation;


void bizop() {
    throw ApplicationException{"bazinga"s};
}

void func() {
    bizop();
}

TEST_CASE("throwing an ApplicationException{}, should produce a decent trace", "[app]") {
    try {
        func();
    } catch (const ApplicationException& err) {
        auto actual = err.stackTrace.callStack;

        auto expected = vector<string>{
                "bizop()"s,
                "func()"s,
                "main()"s,
        };
        REQUIRE_THAT(actual, Catch::Contains(expected));
    }
}

TEST_CASE("throwing an ApplicationException{} and printing the stack-trace", "[app]") {
    try {
        func();
    } catch (const ApplicationException& err) {
        auto buf = std::ostringstream{};
        err.printStackTrace(buf);

        string expected = "1) bizop()\n"s +
                          "2) func()\n"s +
                          "3) Catch::TestInvokerAsFunction::invoke() const\n"s;
        REQUIRE_THAT(buf.str(), Catch::Contains(expected));
    }
}

TEST_CASE("throwing and printing using the << operator", "[app]") {
    try {
        func();
    } catch (const ApplicationException& err) {
        auto buf = std::ostringstream{};
        buf << err;
        string expected = "ApplicationException: bazinga\n"s +
                          "1) bizop()\n"s +
                          "2) func()\n"s +
                          "3) Catch::TestInvokerAsFunction::invoke() const\n"s;
        REQUIRE_THAT(buf.str(), Catch::Contains(expected));
    }
}

