#pragma once

#include <string>
#include <string_view>
#include <iterator>
#include <unordered_map>
#include <vector>
#include <utility>
#include <algorithm>
#include <memory_resource>
#include <thread>
#include <future>
#include <cstdio>

#include "message-queue.hxx"
#include "message.hxx"
#include "receivable.hxx"


namespace ribomation::threads {
    using namespace std::string_view_literals;
    using std::data;
    using std::size;
    using std::thread;
    using std::promise;
    using std::future;
    using std::pair;
    using std::sort;
    using std::min;
    using std::max_element;
    using std::copy_n;
    using std::back_inserter;

    using std::pmr::monotonic_buffer_resource;
    using std::pmr::unsynchronized_pool_resource;
    using std::pmr::null_memory_resource;
    using std::pmr::new_delete_resource;
    using std::pmr::unordered_map;
    using std::pmr::vector;
    using std::pmr::string;

    using ribomation::threads::Receivable;
    using ribomation::threads::MessageQueue;
    using ribomation::threads::Message;


    struct Aggregator : Receivable {
        using MESSAGE = Message<100>;

        Aggregator(unsigned maxWords, Receivable& next) : maxWords(maxWords) , next(next) {}

        void start() {
            thread kicker{&Aggregator::body, this};
            runner.swap(kicker);
            fullyStarted.get_future().wait();
        }

        void join() { runner.join(); }

        void send(unsigned id, string_view data) {
            auto addr = heap->allocate(sizeof(MESSAGE));
            auto msg  = new (addr) MESSAGE{id, data};
            inbox.put(msg);
        }

    private:
        unsigned long maxWords;
        Receivable& next;
        thread                     runner;
        MessageQueue<MESSAGE*, 16> inbox;
        unsynchronized_pool_resource* heap;
        promise<bool> fullyStarted;

        void body() {
            char storage[1'000'000];
            auto buffer = monotonic_buffer_resource{data(storage), size(storage), new_delete_resource()};
            auto pool   = unsynchronized_pool_resource{&buffer};
            heap = &pool;
            fullyStarted.set_value(true);
            run();
        }

        void run() {
            auto freq    = unordered_map<string, unsigned>{heap};
            bool running = true;
            do {
                auto msg = inbox.get();

                auto id = msg->getId();
                if (id == 0) { running = false; }

                auto size = msg->getSize();
                char payload[MESSAGE::MAX_SIZE];
                msg->copyTo(payload, sizeof(payload));

                msg->~MESSAGE();
                heap->deallocate(msg, sizeof(MESSAGE));

                auto word = string{payload, size, heap};
                ++freq[word];
            } while (running);

            auto sortable = vector<pair<string, unsigned >>{freq.begin(), freq.end(), heap};
            sort(sortable.begin(), sortable.end(), [](auto lhs, auto rhs) {
                return lhs.second > rhs.second;
            });

            auto result = vector<pair<string, unsigned >>{heap};
            copy_n(sortable.begin(), min(maxWords, sortable.size()), back_inserter(result));

            auto m = max_element(result.begin(), result.end(), [](auto lhs, auto rhs) {
                return lhs.first.size() < rhs.first.size();
            });
            int maxWidth = m->first.size();

            auto id = 1;
            for (auto const&[word, freq] : result) {
                char buf[512];
                sprintf(buf, "%-*s: %d", maxWidth, word.c_str(), freq);
                next.send(id++, string_view{buf, strlen(buf)});
            }
            next.send(0, ""sv);
        }
    };

}

