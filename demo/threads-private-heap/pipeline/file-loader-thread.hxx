#pragma once

#include <thread>
#include <string_view>
#include <numeric>
#include <iterator>
#include <cstdio>
#include "memory-mapped-file.hxx"
#include "receivable.hxx"


namespace ribomation::threads {
    using namespace std::string_view_literals;
    using std::thread;
    using std::iota;
    using std::begin;
    using std::end;
    using std::string_view;
    using ribomation::files::MemoryMappedFile;
    using ribomation::threads::Receivable;

    struct FileLoader {
        FileLoader(MemoryMappedFile& content, Receivable& next) : content(content), next(next) {}

        void start() {
            thread kicker{&FileLoader::run, this};
            runner.swap(kicker);
        }

        void join() { runner.join(); }

    private:
        MemoryMappedFile& content;
        Receivable      & next;
        thread runner;

        void run() {
            constexpr auto N = 'z' - 'a' + 1;
            char           LETTERS[N * 2];
            iota(begin(LETTERS), end(LETTERS), 'a');
            iota(begin(LETTERS) + N, end(LETTERS), 'A');
            auto letters = string_view{LETTERS, N * 2};

            auto data = content.data();
            auto from = 0UL;
            auto id   = 1;
            do {
                from = data.find_first_of(letters, from);
                if (from == string_view::npos) break;

                auto to = data.find_first_not_of(letters, from);
                if (to == string_view::npos) break;

                auto word = data.substr(from, to - from);
                from = to;

                next.send(id++, word);
            } while (true);
            next.send(0, ""sv);
        }
    };

}
