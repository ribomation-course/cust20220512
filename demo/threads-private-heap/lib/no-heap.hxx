#pragma once
#include <cstddef>
#include <cstdio>
#include <cassert>
#include <new>
#include <stdexcept>

auto operator new(size_t numBytes) -> void* {
    throw std::bad_alloc{};
}

auto operator new[](size_t numBytes) -> void* {
    throw std::bad_alloc{};
}

void ensureNoHeap() {
    try {
        auto p = new int{42};
        assert(p);
        throw std::runtime_error{"unexpected: should throw std::bad_alloc"};
    } catch (std::bad_alloc& x) {
        printf("Confirmed: No heap memory was used!\n");
    }
}
