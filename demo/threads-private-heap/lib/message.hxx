#pragma once
#include <string_view>
#include <algorithm>

namespace ribomation::threads {
    using std::string_view;
    using std::fill_n;
    using std::copy_n;
    using std::min;

    template<unsigned CAPACITY = 32>
    struct Message {
        constexpr static unsigned MAX_SIZE = CAPACITY;

        Message(unsigned id, string_view data)
                : id{id}, size{min(static_cast<unsigned>(data.size()), CAPACITY)}
        {
            fill_n(payload, CAPACITY, '\0');
            copy_n(data.begin(), size, payload);
        }

        Message() = default;
        ~Message() = default;
        Message(const Message<CAPACITY>&) = default;

        unsigned getId()   const { return id; }
        unsigned getSize() const { return size; }

        void copyTo(char* dest, unsigned max) {
            fill_n(dest, max, '\0');
            copy_n(payload, size, dest);
        }
    private:
        unsigned id;
        unsigned size;
        char     payload[CAPACITY];
    };
}


