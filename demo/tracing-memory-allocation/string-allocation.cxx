#include <iostream>
#include <string>
#include "trace-alloc.hxx"
using namespace std;

int main() {
    {
        auto mgr = AllocManager{};
        auto str = string{"Tjabba habba mums filibaba"};
        cout << "str: " << str << " (" << str.size() << " chars)" << "\n";
    }
    cout << "----\n";
    {
        auto mgr = AllocManager{};
        auto str = string{"123456789012345"};
        cout << "str: " << str << " (" << str.size() << " chars)" << "\n";
    }
    cout << "----\n";
    {
        auto mgr = AllocManager{};
        auto str = string{"1234567890123456"};
        cout << "str: " << str << " (" << str.size() << " chars)" << "\n";
    }
    return 0;
}

