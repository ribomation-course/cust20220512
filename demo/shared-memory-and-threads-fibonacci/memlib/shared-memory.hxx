#pragma once

#include <string>
#include <stdexcept>
#include <cmath>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>


class SharedMemory {
    const std::string   name;
    const unsigned size;
    const void* begin;
    const void* end;
    void      * next;

    void sysfail(const std::string& func) {
        std::string msg = func + "() failed: " + strerror(errno);
        throw std::runtime_error(msg);
    }

    unsigned adjustSize(size_t size) {
        const double PS = getpagesize();
        return static_cast<unsigned>(ceil(size / PS) * PS);
    }

    std::string adjustName(const std::string& name) {
        if (name[0] != '/') {
            const_cast<std::string&>(name) = '/' + name;
        }
        return name;
    }

public:
    SharedMemory(size_t size_, const std::string& name_ = "/shmxx", void* startAddress = 0)
            : name{adjustName(name_)}, size{adjustSize(size_)} {
        int fd = shm_open(name.c_str(), O_RDWR | O_CREAT | O_TRUNC, 0660);
        if (fd < 0) sysfail("shm_open");

        int rc = ftruncate(fd, size);
        if (rc < 0) sysfail("ftruncate");

        void* start = mmap(startAddress, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (start == MAP_FAILED) sysfail("mmap");
        close(fd);

        SharedMemory::begin = start;
        SharedMemory::next  = start;
        SharedMemory::end   = start + size;
    }

    ~SharedMemory() {
        munmap(const_cast<void*>(begin), size);
        shm_unlink(name.c_str());
    }

    void* allocateBytes(size_t numBytes) {
        void* addr = next;
        next += numBytes;
        if (end <= next) {
            throw std::overflow_error("shared-memory overflow");
        }
        return addr;
    }

    template<typename T>
    T* allocate(unsigned n = 1) {
        return reinterpret_cast<T*>(allocateBytes(n * sizeof(T)));
    }

    size_t capacity() const { return size; }
    size_t free() const { return (reinterpret_cast<size_t>(end) - reinterpret_cast<size_t>(next)); }

    SharedMemory() = delete;
    SharedMemory(const SharedMemory&) = delete;
    SharedMemory& operator=(const SharedMemory&) = delete;
};
