#pragma once

#include <chrono>
#include "memlib/object-pool.hxx"

using namespace std::chrono;

using XXL = unsigned long long;
constexpr static auto STOP = 0U;

struct Request {
    unsigned                 argument;
    time_point<steady_clock> startTime;

    Request(unsigned int argument)
            : argument{argument}, startTime{steady_clock::now()} {}
};

struct Response {
    unsigned                 argument;
    XXL                      result;
    time_point<steady_clock> startTime;

    Response(unsigned argument, XXL result, time_point<steady_clock> t)
            : argument{argument}, result{result}, startTime{t} {}

    auto elapsed() {
        auto endTime = steady_clock::now();
        return duration_cast<milliseconds>(endTime - startTime).count();
    }
};

using RequestPool = Pool<Request, 256>;
using RequestQueue = MessageQueue<Request*>;

using ResponsePool = Pool<Response, 256>;
using ResponseQueue = MessageQueue<Response*>;
