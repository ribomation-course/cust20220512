#pragma once
#include "mutex.hxx"

class Guard {
    Mutex& mutex;

public:
    Guard(Mutex& mutex) : mutex(mutex) {
        mutex.lock();
    }

    ~Guard() {
        mutex.unlock();
    }

    Guard() = delete;
    Guard(const Guard&) = delete;
    Guard& operator=(const Guard&) = delete;
};


