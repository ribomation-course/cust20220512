cmake_minimum_required(VERSION 3.16)
project(user_defined_memory_allocation)

set(CMAKE_CXX_STANDARD 17)
set(NOWARN -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable -Wno-pointer-arith)
set(WARN -Wall -Wextra -Wfatal-errors ${NOWARN})


add_executable(tst-static
        src/data.hxx
        lib/no-heap.hxx
        lib/static-block-pool.hxx
        lib/smart-pointer-support.hxx
        src/tst-static-pool.cxx
        )
target_compile_options(tst-static PRIVATE ${WARN})
target_include_directories(tst-static PRIVATE lib src)


add_executable(tst-static-2
        src/data.hxx
        lib/no-heap.hxx
        lib/static-block-pool.hxx
        lib/smart-pointer-support.hxx
        src/tst-static-pool-2.cxx
        )
target_compile_options(tst-static-2 PRIVATE ${WARN})
target_include_directories(tst-static-2 PRIVATE lib src)


add_executable(tst-static-3
        src/data.hxx
        lib/no-heap.hxx
        lib/static-block-pool.hxx
        lib/smart-pointer-support.hxx
        src/tst-static-pool-3.cxx
        )
target_compile_options(tst-static-3 PRIVATE ${WARN})
target_include_directories(tst-static-3 PRIVATE lib src)


add_executable(tst-dynamic
        lib/dynamic-block-pool.hxx
        lib/no-heap.hxx
        src/data.hxx
        src/tst-dynamic-pool.cxx
        )
target_compile_options(tst-dynamic PRIVATE ${WARN})
target_include_directories(tst-dynamic PRIVATE lib src)

