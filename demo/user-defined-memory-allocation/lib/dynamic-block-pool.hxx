#include <vector>
#include <stdexcept>
#include <algorithm>

namespace ribomation::memory {
    using namespace std;
    using byte = unsigned char;

    template<typename Type>
    struct DynamicBlockPool {
        DynamicBlockPool(void* storage, unsigned storageSize) :
                storageSize(storageSize),
                numBlocks(storageSize / sizeof(Type)),
                storage(storage),
                usage{} {
            usage.reserve(numBlocks);
            for (auto k = 0U; k < numBlocks; ++k) usage.push_back(false);
        }

        auto allocate() -> Type* {
            if (full()) throw overflow_error{"pool full"};
            auto idx = nextIndex();
            usage[idx] = true;
            return toAddress(idx);
        }

        void deallocate(Type* ptr) {
            if (isMember(ptr)) {
                auto idx = toIndex(ptr);
                usage[idx] = false;
            } else {
                throw out_of_range{"not from this pool"};
            }
        }

        unsigned allocated() const {
            return count_if(begin(usage), end(usage), [](auto inUse) { return inUse; });
        }
        unsigned available() const { return numBlocks - allocated(); }
        bool full() const { return available() == 0; }

        ~DynamicBlockPool() = default;
        DynamicBlockPool() = delete;
        DynamicBlockPool(const DynamicBlockPool<Type>&) = delete;
        auto operator =(const DynamicBlockPool<Type>&)
                -> DynamicBlockPool<Type>& = delete;

    private:
        constexpr static size_t blockSize = sizeof(Type);
        unsigned const storageSize;
        unsigned const numBlocks;
        void* const storage;
        vector<bool> usage;

        unsigned nextIndex() const {
            unsigned index = 0;
            while (index < usage.size() && usage.at(index)) ++index;
            return index;
        }

        Type* toAddress(unsigned index) {
            auto begin  = reinterpret_cast<byte*>(storage);
            auto offset = index * blockSize;
            return reinterpret_cast<Type*>(begin + offset);
        }

        unsigned toIndex(Type* ptr) const {
            auto const begin = reinterpret_cast<unsigned long>(storage);
            auto const addr  = reinterpret_cast<unsigned long>(ptr);
            auto const index = (addr - begin) / blockSize;
            return static_cast<unsigned>(index);
        }

        bool isMember(Type* ptr) const {
            auto addr  = reinterpret_cast<byte*>(ptr);
            auto begin = reinterpret_cast<byte*>(storage);
            auto end   = begin + storageSize;
            return (begin <= addr) && (addr < end);
        }
    };

}

