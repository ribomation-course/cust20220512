#pragma once

#include <bitset>
#include <stdexcept>

namespace ribomation::memory {

    template<typename Type, unsigned numBlocks>
    struct StaticBlockPool {
        auto allocate() -> Type* {
            if (full()) throw std::overflow_error{"pool full"};
            auto idx = nextIndex();
            usage.set(idx);
            return toAddress(idx);
        }

        void deallocate(Type* ptr) {
            if (isMember(ptr)) {
                auto idx = toIndex(ptr);
                usage.reset(idx);
            } else {
                throw std::out_of_range{"not from this pool"};
            }
        }

        unsigned allocated() const { return usage.count(); }

        unsigned available() const { return numBlocks - allocated(); }

        bool full() const { return available() == 0; }

        StaticBlockPool() = default;
        ~StaticBlockPool() = default;
        StaticBlockPool(const StaticBlockPool<Type, numBlocks>&) = delete;
        auto operator =(const StaticBlockPool<Type, numBlocks>&)
        -> StaticBlockPool<Type, numBlocks>& = delete;

    private:
        using byte = unsigned char;
        constexpr static size_t blockSize   = sizeof(Type);
        constexpr static size_t storageSize = numBlocks * blockSize;

        byte                   storage[storageSize];
        std::bitset<numBlocks> usage;

        unsigned nextIndex() const {
            unsigned index = 0;
            while (index < usage.size() && usage.test(index)) ++index;
            return index;
        }

        Type* toAddress(unsigned index) {
            return reinterpret_cast<Type*>(storage + index * blockSize);
        }

        unsigned toIndex(Type* ptr) const {
            auto const begin = reinterpret_cast<unsigned long>(storage);
            auto const addr  = reinterpret_cast<unsigned long>(ptr);
            auto const index = (addr - begin) / blockSize;
            return static_cast<unsigned>(index);
        }

        bool isMember(Type* ptr) const {
            auto addr  = reinterpret_cast<byte*>(ptr);
            auto begin = storage;
            auto end   = begin + storageSize;
            return (begin <= addr) && (addr < end);
        }
    };

}
