#pragma once

#include <memory>
#include "static-block-pool.hxx"

namespace ribomation::memory {

    template<typename BlockType, unsigned numBlocks>
    struct PoolDeleter {
        void operator ()(BlockType* ptr) {
            ptr->~BlockType();
            pool->deallocate(ptr);
        }

        PoolDeleter(StaticBlockPool <BlockType, numBlocks>* pool) : pool(pool) {}

        PoolDeleter() = default;
        PoolDeleter(PoolDeleter<BlockType, numBlocks>&& that) = default;
        PoolDeleter(const PoolDeleter<BlockType, numBlocks>&) = default;
        auto operator =(PoolDeleter<BlockType, numBlocks>&& that)
        -> PoolDeleter<BlockType, numBlocks>& = default;
        auto operator =(const PoolDeleter<BlockType, numBlocks>& that)
        -> PoolDeleter<BlockType, numBlocks>& = default;

    private:
        StaticBlockPool <BlockType, numBlocks>* pool;
    };

    template<typename Type, unsigned Capacity>
    using UniquePtr = std::unique_ptr<Type, PoolDeleter<Type, Capacity>>;

    template<typename Type, unsigned Capacity, typename ...ConstructorArgs>
    auto make_unique(StaticBlockPool<Type, Capacity>& pool, ConstructorArgs ...args)
        -> UniquePtr<Type, Capacity>
    {
        auto obj     = new (pool.allocate()) Type{args...};
        auto deleter = PoolDeleter<Type, Capacity>{&pool};
        return UniquePtr<Type, Capacity>{obj, deleter};
    }

}

