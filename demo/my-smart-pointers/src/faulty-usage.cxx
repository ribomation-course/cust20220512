#include <iostream>
#include <sstream>
#include <string>

#include "simple-ptr.hxx"
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::ptr;
using namespace ribomation::persons;

struct Car {
    Car(string regno, Person* owner) : regno{regno}, owner{owner} {}
    auto toString() {
        auto buf = ostringstream{};
        buf << "Car{" << regno << ", " << owner->toString() << "} @ " << this;
        return buf.str();
    }
private:
    const string      regno;
    SimplePtr<Person> owner;
};


int main() {
    cout << "[main] enter\n";
    auto nisse = new Person{"Nisse", 23};
    cout << "person: " << nisse->toString() << "\n";
    {
        auto volvo = Car{"ABC123", nisse};
        cout << "car: " << volvo.toString() << "\n";
    }
    cout << "[main] ----\n";
    cout << "person: " << nisse->toString() << "\n";
    cout << "[main] exit\n";
    return 0;
}

