#include <iostream>
#include <string>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::persons;

int main() {
    auto p = Person{"Anna Conda"s, 42};
    cout << "p: " << p.toString() << endl;
    p.incrAge();
    cout << "p: " << p.toString() << endl;
    return 0;
}
