#pragma once

#include <string>
#include <stdexcept>

namespace ribomation::ptr {
    using namespace std;
    using namespace std::literals;

    struct NullPtrException : runtime_error {
        NullPtrException() : runtime_error{"null-pointer exception"s} {}
    };

    template<typename Type>
    struct MonoPtr {
        using Addr = Type*;

        MonoPtr() : addr{nullptr} {}
        MonoPtr(Addr addr) : addr{addr} {}
        ~MonoPtr() { if (addr != nullptr) delete addr; }

        MonoPtr(const MonoPtr<Type>& that) : addr{that.addr} {
            const_cast<MonoPtr<Type>&>(that).addr = nullptr;
        }

        MonoPtr(MonoPtr<Type>&& that) noexcept: addr{that.addr} {
            that.addr = nullptr;
        }

        auto operator =(const MonoPtr<Type>& that) -> MonoPtr<Type>& {
            if (this != &that) {
                this->addr                            = that.addr;
                const_cast<MonoPtr<Type>&>(that).addr = nullptr;
            }
            return &this;
        }

        auto operator =(MonoPtr<Type>&& that) noexcept -> MonoPtr<Type>& {
            if (this != &that) {
                this->addr = that.addr;
                that.addr  = nullptr;
            }
            return &this;
        }

        auto operator ->() const -> Addr {
            if (addr != nullptr) return addr;
            throw NullPtrException{};
        }

        auto operator *() const -> Type& {
            if (addr != nullptr) return *addr;
            throw NullPtrException{};
        }

        operator bool() const {
            return addr != nullptr;
        }

        auto release() -> Addr {
            auto x = addr;
            addr = nullptr;
            return x;
        }

        auto operator &() -> Addr = delete;

    private:
        Addr addr;
    };


    template<typename Type, typename ...Args>
    auto make_mono_ptr(Args ...args) -> MonoPtr<Type> {
        auto adr = new unsigned char[sizeof(Type)];
        auto obj = new (adr) Type{args...};
        return MonoPtr<Type>{obj};
    }

}
