#include <iostream>
#include <string>
#include <vector>
#include "person.hxx"
#include "mono-ptr.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::persons;
using namespace ribomation::ptr;

void test_single() {
    auto t = Trace{"single ptr"s};
    auto p = MonoPtr<Person>{new Person{"Anna Conda"s, 42}};
    t.out() << "p: " << p->toString() << endl;
    (*p).incrAge();
    t.out() << "p: " << p->getName() << ", " << p->getAge() << endl;
}

void test_vector_of_ptr() {
    auto t = Trace{"vector of ptr"s};
    auto vec = vector<MonoPtr<Person>>{
            new Person{"Anna Conda"s, 42},
            new Person{"Justin Time"s, 33},
            new Person{"Per Silja"s, 57},
    };
    for (auto& p : vec) p->incrAge();
    for (auto& p : vec) t.out() << "p: " << p->toString() << endl;
}

void test_nullptr() {
    auto t = Trace{"nullptr"s};
    auto ptr = MonoPtr<int>{};
    try {
        *ptr; //boom
    } catch (NullPtrException& x) {
        t.out() << " !! " << x.what() << endl;
    }
}

void compiler_error() {
    auto t = Trace{"compilation error"s};
    auto ptr = MonoPtr<int>{new int{42}};
    //auto x = &ptr;
    //error: use of deleted function Type* MonoPtr<Type>::operator&() [with Type = int; MonoPtr<Type>::Addr = int*]
}

MonoPtr<Person> func(MonoPtr<Person> ptr) {
    auto t = Trace{"func"s};
    ptr->incrAge();
    t.out() << "ptr: " << ptr->toString() << endl;
    return ptr;
}

void test_func_call() {
    auto t = Trace{"func call"s};
    auto p = MonoPtr<Person>{new Person{"Anna Conda"s, 42}};
    t.out() << "p: " << p->toString() << endl;

    auto q = func(p);
    t.out() << "p: " << (p ? p->toString() : "<null>") << endl;
    t.out() << "q: " << (q ? q->toString() : "<null>") << endl;
}

void test_release() {
    auto t = Trace{"release"s};
    auto ptr = MonoPtr<Person>{new Person{"Anna Conda"s, 42}};
    t.out() << "ptr: " << ptr->toString() << endl;

    auto raw = ptr.release();
    t.out() << "raw: " << raw->toString() << endl;
    t.out() << "ptr: " << (ptr ? ptr->toString() : "<null>") << endl;
    delete raw;
}

void test_factory_func() {
    auto t = Trace{"factory"s};
    auto ptr = make_mono_ptr<Person>("Inge Vidare", 42);
    t.out() << "ptr: " << ptr->toString() << endl;
}

int main() {
    auto t = Trace{"main"s};
    test_single();
    t.out() << "----\n";
    test_vector_of_ptr();
    t.out() << "----\n";
    compiler_error();
    t.out() << "----\n";
    test_nullptr();
    t.out() << "----\n";
    test_func_call();
    t.out() << "----\n";
    test_release();
    t.out() << "----\n";
    test_factory_func();
    cout << "All tests passed\n";
    return 0;
}
